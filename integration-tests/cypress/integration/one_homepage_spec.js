/// <reference types="Cypress" />

describe('One App test', function() {
    let polyfill
    // grab fetch polyfill from remote URL, could be also from a local package
    before(() => {
        const polyfillUrl = 'https://unpkg.com/unfetch/dist/unfetch.umd.js'
        cy.request(polyfillUrl)
            .then((response) => {
                polyfill = response.body
            })
    });
    beforeEach(function () {
        // all calls will be done via XHR after we load polyfill
        // so we can spy on them using cy.route
        cy.server()
        cy.route('/clicks').as('fetchClicks')
        cy.route('/click').as('insertClick')

        // We use cy.visit({onBeforeLoad: ...}) to delete native fetch and load polyfill code instead
        cy.visit('https://front-gkguafzudo.now.sh/', {
            onBeforeLoad (win) {
                delete win.fetch;
                // since the application code does not ship with a polyfill
                // load a polyfilled "fetch" from the test
                win.eval(polyfill)
                win.fetch = win.unfetch
            },
        })
    });

    // it('Test the click button', function() {
    //     cy.server();
    //     cy.route('/clicks').as('fetchClicks')
    //
    //     cy.visit("https://front-gkguafzudo.now.sh/");
    //     cy.wait('@fetchClicks');
    //     cy.get('#clickCount').invoke('text');
    //
    // });

    it('Test the presence of a text on the homepage', function() {
        cy.visit("https://front-gkguafzudo.now.sh/");
        cy.contains('the only app you need')

    });

});
